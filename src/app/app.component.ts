import {Component, OnInit} from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  title = 'frontend';

  public result: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
     this.http.get('http://127.0.1:8000/api/user').subscribe(data => {
       this.result = data;
     });
  }

}
